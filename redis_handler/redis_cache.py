
import traceback

from redis_handler.redis_handler import RedisHandler


def set_cache_data(key, data, timeout=36000):
    try:
        rc = RedisHandler.get_instance(hostname='ENTITY_REDIS_HOST')
        rc.set(key,data)
    except Exception as e:
        raise Exception('Operation failed while setting key in redis')

    return True


def get_cached_data(key):
    try:
        return RedisHandler.get_instance(hostname='ENTITY_REDIS_HOST').get(key)

    except Exception as e:
        return None


def delete_cached_data(key):
    try:
        if type(key) == list:
            return RedisHandler.get_instance(hostname='ENTITY_REDIS_HOST').delete_keys(key)
        else:
            return RedisHandler.get_instance(hostname='ENTITY_REDIS_HOST').delete_keys([key])

    except Exception as e:
        return None
