import json
import traceback
import os

import redis
from redis import RedisError


class CacheHandlerBase:

    @staticmethod
    def initialize():
        raise NotImplementedError("you need to implement this before using")

    @staticmethod
    def get_json_value(key):
        raise NotImplementedError("you need to implement this before using")

    @staticmethod
    def set(key, value, timeout=200, set_timeout=True):
        raise NotImplementedError("you need to implement this before using")

    @staticmethod
    def get_raw_value(key):
        raise NotImplementedError("you need to implement this before using")

    @staticmethod
    def set_raw_value(key, value, timeout=200, set_timeout=True):
        raise NotImplementedError("you need to implement this before using")

    @staticmethod
    def delete_keys(keys):
        raise NotImplementedError("you need to implement this before using")

# CACHE_SERVER = None
microservice_name = os.getenv("PROJECT_NAME", "unknown")
REDIS_TIMEOUT = int(os.getenv("REDIS_TIMEOUT", 3600))

class RedisHandler(CacheHandlerBase):
    __instance__ = None
    __instances__ = dict()
    __host__ = None
    __connection__ = None

    def __init__(self, hostname=None):

        try:
            if hostname:
                try:
                    host = os.getenv(hostname, "localhost")
                except KeyError:
                    host = os.getenv("REDIS_HOST", "localhost")
            else:
                host = os.getenv("REDIS_HOST", "localhost")

            port = int(os.getenv("REDIS_NOAUTH_PORT", "6379"))

            if hostname == "ASSET_REDIS_HOST":
                port = int(os.getenv("ASSET_REDIS_NOAUTH_PORT", "6379"))

            max_connections = int(os.getenv("REDIS_MAX_CONNECTIONS", "5"))

            pool = redis.ConnectionPool(host=host,
                                                port=port, max_connections=max_connections)
            RedisHandler.__connection__ = redis.Redis(connection_pool=pool)
            self.connection = RedisHandler.__connection__
            RedisHandler.__host__ = hostname
            RedisHandler.__instance__ = self
            RedisHandler.__instances__[hostname] = self

        except KeyError as e:
            error_message = "key not found error: " + str(e)
            tb = traceback.format_exc()
            raise RedisError(error_message, traceback=tb)

        except Exception as e:
            error_message = str(e)
            tb = traceback.format_exc()
            microservice_name = os.getenv("PROJECT_NAME", "unknown")
            raise RedisError(error_message, traceback=tb)

    @staticmethod
    def get_instance(hostname=None):

        if hostname in RedisHandler.__instances__:
            RedisHandler.__instance__ = RedisHandler.__instances__[hostname]
            RedisHandler.__host__ = hostname
            RedisHandler.__connection__ = RedisHandler.__instance__.connection
        else:
            RedisHandler(hostname)
        return RedisHandler.__instance__

    def get_json_value(self, key, hostname=None):
        value = self.__connection__.get(key)
        if value is not None:
            value = json.loads(value.decode())
        return value

    def set(self, key, value, timeout=REDIS_TIMEOUT, set_timeout=True):
        if set_timeout:
            self.__connection__.set(key, json.dumps(value), timeout)
        else:
            self.__connection__.set(key, json.dumps(value))

    def get_raw_value(self, key):
        return self.__connection__.get(key).decode()[1:-1]

    def set_raw_value(self, key, value, timeout=REDIS_TIMEOUT, set_timeout=True):
        if set_timeout:
            self.__connection__.set(key, json.dumps(value), timeout)
        else:
            self.__connection__.set(key, json.dumps(value))

    def get_keys(self, pattern=None):
        if pattern:
            keys = self.__connection__.keys(pattern)
        else:
            keys = self.__connection__.keys()
        return [key.decode('utf-8') for key in keys if key]

    def delete_keys(self, keys):
        self.__connection__.delete(*keys)

    def hmset(self, name, json_obj):
        self.__connection__.hmset(name, json_obj)

    def exists(self, name):
        return self.__connection__.exists(name)

    def expire(self, name, timeout=REDIS_TIMEOUT):
        self.__connection__.expire(name, timeout)

    def hmget(self, name, keys):
        v = self.__connection__.hmget(name, keys)
        if v is not None:
            return [str(x, 'utf-8') if x is not None else None for x in v]
        return None

    def hget(self, name, key):
        v = self.__connection__.hmget(name, key)[0]
        return str(v, 'utf-8') if v is not None else None

    def hset(self, name, key, value):
        return self.__connection__.hset(name, key, value)

    def hincrby(self, name, key, value):
        return self.__connection__.hincrby(name, key, value)

    def hgetall(self, name):
        return {str(k, 'utf-8'): str(v, 'utf-8') for k, v in self.__connection__.hgetall(name).items()}

    def llen(self, name):
        return self.__connection__.llen(name)

    def linsert(self, name, where, refvalue, value):
        return self.__connection__.linsert(name, where, refvalue, value)

    def lindex(self, name, index):
        return self.__connection__.lindex(name, index)

    def rpush(self, name, *values):
        return self.__connection__.rpush(name, *values)

    def lpush(self, name, *values):
        return self.__connection__.lpush(name, *values)

    def lrange(self, name, start, end):
        return [str(k, 'utf-8') for k in self.__connection__.lrange(name, start, end)]

    def lpop(self, name):
        return self.__connection__.lpop(name)

    def rpop(self, name):
        return self.__connection__.rpop(name)

    def lrem(self, name, count, value):
        return self.__connection__.lrem(name, count, value)

    def lset(self, name, index, value):
        return self.__connection__.lset(name, index, value)

    def ltrim(self, name, start, end):
        return self.__connection__.ltrim(name, start, end)

    def lock(self, name, timeout=None, sleep=0.1, blocking_timeout=None, lock_class=None, thread_local=True):
        return self.__connection__.lock(name, timeout, sleep, blocking_timeout, lock_class, thread_local)

    def hdelete(self, name, *keys):
        return self.__connection__.hdel(name, *keys)

    def scan_keys(self, match_pattern=None, count=1000):
        return [str(k, 'utf-8') for k in self.__connection__.scan_iter(match=match_pattern, count=count)]

    def mget(self, keys, only_values=True, *args):
        if not keys:
            return [] if only_values else {}
        values = self.__connection__.mget(keys, *args)
        if only_values:
            return [json.loads(v.decode()) for v in values]
        else:
            return {k: json.loads(v.decode()) for k, v in zip(keys, values)}

    def scan(self, match_pattern=None, count=1000, only_values=True):
        return self.mget(self.scan_keys(match_pattern, count), only_values)

    def rename(self, src_key, dst_key):
        return self.__connection__.rename(src_key, dst_key)

    def update(self, src_key, dst_key, value, timeout=1000, set_timeout=False):
        self.rename(src_key, dst_key)
        return self.set(key=dst_key, value=value, timeout=timeout, set_timeout=set_timeout)
