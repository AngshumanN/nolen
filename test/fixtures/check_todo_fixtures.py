import pytest


@pytest.fixture(params=[True], scope="function")
def check_todo_msg(request):
    msg = {
        "data":
            {
                "todo": "this is first todo",
                "checked": request.param
            }
    }
    return msg
