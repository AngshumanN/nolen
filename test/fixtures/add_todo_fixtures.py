import json
import os

import pytest

#
# @pytest.fixture(params=["this is first todo", "this is another todo"], scope="function")
# def get_add_message(request):
#     msg = {
#         "data":
#             {
#                 "todo": request.param
#             }
#     }
#     return msg


@pytest.fixture(params=["this is first todo", "this is another todo"], scope="function")
def get_add_message(request):
    with open("/home/xpms/workspace_an/projects/testing-train/nolen/test/triggers/test_cases/add_todo.json") as data_file:
        data = json.load(data_file)
    data.update({
        "data":
            {
                "todo": request.param
            }
    })
    return data
