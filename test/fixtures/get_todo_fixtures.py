import pytest


@pytest.fixture(params=["all", "checked", "unchecked"], scope="function")
def get_todo_msg(request):
    msg = {
        "data":
            {
                "get": request.param
            }
    }
    return msg
