import mock
import pytest
from pytest_mock import mocker

from db_handler.mongo import MongoConnection
from microservice.triggers import add_todo
from test.fixtures.add_todo_fixtures import get_add_message

import mongomock
from microservice.triggers.add_todo import AddTodoTrigger


# fixture example
@mock.patch.object(MongoConnection, 'get_client', return_value=mongomock.MongoClient())
def test_add_todo_fix(mocked_mongo, get_add_message):
    dd = AddTodoTrigger()
    res = dd.run(msg=get_add_message)
    assert res == get_add_message["data"]["todo"]


# parameterize example with mongo mock
@mock.patch.object(MongoConnection, 'get_client', return_value=mongomock.MongoClient())
@pytest.mark.parametrize("test_input,expected", [
    ("the quick brown fox jumps over the lazy dog", "the quick brown fox jumps over the lazy dog")])
def test_add_todo(mocked_mongo, test_input, expected):
    todo = AddTodoTrigger().add_todo(test_input)
    assert todo == expected


# parameterize with multiple inputs
@pytest.mark.parametrize("test_input,expected",
                         [("the quick brown fox jumps over the lazy dog", 43), ("another test", 12)])
def test_return_length(test_input, expected):
    todo = AddTodoTrigger().return_length(test_input)
    assert todo == expected
