import fakeredis
import mock

from microservice.triggers.redis_mock import RedisObject
from redis_handler.redis_handler import RedisHandler
from pytest_mock import mocker

server = fakeredis.FakeServer()
rc = fakeredis.FakeStrictRedis(server=server)


# redis mock example
@mock.patch.object(RedisHandler, 'get_instance', return_value=rc)
def test_set_data(mocked_redis):
    dd = RedisObject()
    res = dd.set_data("ping", "pong")
    v = dd.get_data(res)
    assert v.decode('utf8') == "pong"
