import mock
import pytest
from pytest_mock import mocker

from microservice.triggers.sample_pymock import SamplePymock, MockedSamplePymock, call_the_class


# stub a class method example
def test_to_test_pymock(mocker):
    mocker.patch('microservice.triggers.sample_pymock.SamplePymock.intermediate_method_to_stub', return_value=True)
    dd = SamplePymock()
    res = dd.to_test_pymock()
    assert res == "mock success"


# stub a class example
def test_to_test_pymock_class(mocker):
    mocker.patch('microservice.triggers.sample_pymock.SamplePymock', MockedSamplePymock)
    dd = call_the_class()
    assert dd == "mocked"
