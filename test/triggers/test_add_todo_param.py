import json
import os

import mock
import pytest

from db_handler.mongo import MongoConnection

import mongomock
from microservice.triggers.add_todo import AddTodoTrigger


# parametrize using json files as test_cases example

def pytest_generate_tests(metafunc):
    file_name = os.path.basename(__file__)
    abspath = os.path.abspath(__file__)
    dir_name = os.path.dirname(abspath)
    dir_name = dir_name + "/test_cases/"
    test_path = dir_name + file_name
    test_path_parts = test_path.split(".")
    for parameter_name in ["create_todo_test"]:
        if parameter_name in metafunc.fixturenames:
            tests_file = test_path_parts[0] + "/" + parameter_name + ".json"

            with open(tests_file) as data_file:
                data = json.load(data_file)
                metafunc.parametrize(parameter_name, data["test_cases"])


@pytest.mark.run(order=1)
@mock.patch.object(MongoConnection, 'get_client', return_value=mongomock.MongoClient())
def test_add_todo(mocked_mongo, create_todo_test):
    request_data = create_todo_test['request_data']
    dd = AddTodoTrigger()
    res = dd.run(msg=request_data)
    assert res == create_todo_test["expected"]
