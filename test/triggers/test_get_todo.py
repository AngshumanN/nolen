import mock
import pytest
from pytest_mock import mocker

from db_handler.mongo import MongoConnection
from test.fixtures.get_todo_fixtures import get_todo_msg
from test.fixtures.add_todo_fixtures import get_add_message
from test.fixtures.check_todo_fixtures import check_todo_msg
import mongomock

from microservice.triggers.get_todo import GetTodoTrigger
from microservice.triggers.add_todo import AddTodoTrigger
from microservice.triggers.check_todo import CheckTodoTrigger


# multiple fixtures in same test example

@pytest.mark.run(order=6)
@mock.patch.object(MongoConnection, 'get_client', return_value=mongomock.MongoClient())
def test_check_todo(mocked_mongo, get_todo_msg, check_todo_msg, get_add_message):
    aa = AddTodoTrigger()
    res = aa.run(msg=get_add_message)
    bb = CheckTodoTrigger()
    res = bb.run(msg=check_todo_msg)
    dd = GetTodoTrigger()
    res = dd.run(msg=get_todo_msg)
    assert len(res) != 0
