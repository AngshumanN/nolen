import mock
import pytest
from pytest_mock import mocker

from db_handler.mongo import MongoConnection

import mongomock
from microservice.triggers.check_todo import CheckTodoTrigger


@pytest.mark.run(order=2)
@mock.patch.object(MongoConnection, 'get_client', return_value=mongomock.MongoClient())
def test_check_todo(mocked_mongo, check_todo_msg):
    dd = CheckTodoTrigger()
    res = dd.run(msg=check_todo_msg)
    assert res == check_todo_msg["data"]["todo"]
