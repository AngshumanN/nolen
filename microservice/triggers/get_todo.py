from db_handler.mongo import MongoConnection


class GetTodoTrigger(object):
    def run(self, msg):
        all_todo = []
        filter = msg["data"].get("get", "all")
        if filter == "all":
            all_todo = self.get_todo()
        elif filter == "checked":
            all_todo = self.get_checked_todo()
        elif filter == "unchecked":
            all_todo = self.get_unchecked_todo()
        return all_todo

    def get_todo(self):
        db = MongoConnection()
        filter_obj = {"is_deleted": False}
        all_todo = db.find_all(filter_obj)
        return all_todo

    def get_checked_todo(self):
        db = MongoConnection()
        filter_obj = {"checked": True}
        all_todo = db.find_all(filter_obj)
        return all_todo

    def get_unchecked_todo(self):
        db = MongoConnection()
        filter_obj = {"checked": False}
        all_todo = db.find_all(filter_obj)
        return all_todo

if __name__ == "__main__":
    msg = {"data": {"get": "unchecked"}}
    adt = GetTodoTrigger()
    print(adt.run(msg))
