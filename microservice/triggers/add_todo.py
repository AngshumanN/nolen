from db_handler.mongo import MongoConnection


class AddTodoTrigger(object):
    def run(self, msg):
        todo = msg['data']["todo"]
        t = self.add_todo(todo)
        return t

    def add_todo(self, todo):
        rows = [{'todo': todo, "checked": False, 'is_deleted': False}]
        db = MongoConnection()
        db.insert(rows=rows)
        return todo

    def return_length(self, msg):
        return len(msg)

if __name__ == "__main__":
    msg = {"data": {"todo": "this is a new todo"}}
    adt = AddTodoTrigger()
    adt.run(msg)
