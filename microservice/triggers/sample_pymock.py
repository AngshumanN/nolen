class SamplePymock(object):
    value = None

    def __init__(self):
        self.value = "not_mocked"

    def intermediate_method_to_stub(self):
        # some code which might fail in test environment
        return 6 / 0

    def to_test_pymock(self):
        if self.intermediate_method_to_stub():
            return "mock success"


class MockedSamplePymock(object):
    value = None

    def __init__(self):
        self.value = "mocked"

    def to_test_pymock(self):
        return "mock success"


def call_the_class():
    dd = SamplePymock()
    return dd.value
