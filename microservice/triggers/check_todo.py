from db_handler.mongo import MongoConnection


class CheckTodoTrigger(object):
    def run(self, msg):
        todo = msg["data"]["todo"]
        chk = msg["data"].get("checked", True)
        self.check_todo(todo) if chk else self.uncheck_todo(todo)
        return todo

    def check_todo(self, todo):
        db = MongoConnection()
        filter_obj = {'todo': todo}
        db.update(filter=filter_obj, update_row=dict(checked=True))

    def uncheck_todo(self, todo):
        db = MongoConnection()
        filter_obj = {'todo': todo}
        db.update(filter=filter_obj, update_row=dict(checked=False))

if __name__ == "__main__":
    msg = {"data": {"todo": "this is a new todo"}}
    adt = CheckTodoTrigger()
    adt.run(msg)
