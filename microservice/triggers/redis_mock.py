from redis_handler.redis_cache import set_cache_data, get_cached_data


class RedisObject(object):

    def set_data(self, key, data):
        set_cache_data(key, data)
        return key

    def get_data(self, key):
        return get_cached_data(key)
