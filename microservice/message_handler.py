import inspect
import json
import os
import traceback
from uuid import uuid4
from microservice.triggers.add_todo import AddTodoTrigger
from microservice.triggers.check_todo import CheckTodoTrigger
from microservice.triggers.get_todo import GetTodoTrigger


class MessageHandler:
    @staticmethod
    def handle_message(message_json):
        MessageHandler.get_trigger_handler(message_json)

    @staticmethod
    def get_trigger_handler(msg_dict):
        try:
            process_id = str(uuid4())
            msg_dict = json.loads(msg_dict)
            payload = msg_dict['payload']
            trigger = payload['trigger']
            request_context = dict() if 'context' not in payload else payload['context']
            trigger_handler = None
        except Exception as e:
            raise e
        try:
            if trigger == "add_todo":
                trigger_handler = AddTodoTrigger(msg_dict)
            if trigger == "check_todo":
                trigger_handler = CheckTodoTrigger(msg_dict)
            if trigger == "get_todo":
                trigger_handler = GetTodoTrigger(msg_dict)

            # if trigger_handler is None:
            #     trigger_handler = TriggerHandler(context, msg_dict)
            #     trigger_handler.do_not_respond()

        except Exception as e:
            trigger_handler.response.do_not_respond = True

        if not trigger_handler.response.do_not_respond:

            if trigger_handler.response.routing_key is None:
                trigger_handler.response.routing_key = trigger_handler.response.message_type + "." + trigger_handler.response.trigger

            trigger_handler.response.publish()
