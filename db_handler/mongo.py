import pymongo


class MongoConnection(object):
    client = None
    db = None
    col = None
    username = "root"
    password = "pa88w0rd"
    host = "mongo"
    port = "27017"
    source_db = "admin"
    max_pool_size = "20"
    min_pool_size = "2"

    def __init__(self):
        self.client = self.get_client()
        self.db = self.client["dev8_admin"]
        self.col = self.db["todo"]

    def get_client(self):
        return pymongo.MongoClient(**self.gen_con_config())

    def insert(self, rows):
        rows = [rows] if not isinstance(rows, list) else rows
        return self.col.insert_many(rows)

    def update(self, update_row, filter):
        newvalues = {"$set": update_row}
        self.col.update_one(filter, newvalues)
        return "done"

    def find_all(self,filter_obj={}):
        all_res = []
        for x in self.col.find(filter_obj):
            all_res.append(x)
        return all_res

    def gen_con_config(self):
        settings_mongo = {
            'username': self.username,
            'password': self.password,
            'host': self.host + ":" +
                    self.port,
            'source_database': self.source_db
        }

        connection_config = {
            "host": "mongodb://{username}:{password}@"
                    "{host}/?authSource={source_database}".format(
                **settings_mongo), "maxPoolSize": self.max_pool_size, "minPoolSize": self.min_pool_size
        }
        return connection_config
